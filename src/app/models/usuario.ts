import { Materia } from "./materia";
import { Tempo } from './tempo';

export class Usuario extends Tempo {
    id?: any;
    nome?: string;
    matricula?: string;
    token?: any;
    aniversario?: Date;
    materiasRealizadas?: Array<Materia>;
    materiasMatriculadas?: Array<Materia>;
    desempenho?: number;
    faltas?: number;
    desempenhoDiario?: Array<number>;
    aulaHoje: boolean;
    // cansaco?: number;
    ultimaAcao?: {
        key: string;
        horasDeEstudo?: number;
        horasDeLazer?: number;
        horasDeSono?: number;
        horasDeAula?: number;
        pausa?: boolean;
    }
    historicoDeAcoes?: Array<any>;
    public super?(name, matricula, aniversario, desempenho, tempo, materiasRealizadas?, materiasMatriculadas?) {
        this.nome = name;
        this.matricula = matricula;
        this.aniversario = aniversario;
        this.desempenho = desempenho;
        this.setTime(tempo);
        // this.cansaco = cansaco;
        if (materiasMatriculadas) {
            this.materiasMatriculadas = materiasMatriculadas;
        }
        if (materiasRealizadas) {
            this.materiasRealizadas = materiasRealizadas;
        }
        this.historicoDeAcoes = [];

    }
    public setTime(tempo: Tempo) {
        console.log(tempo);

        this.dia = tempo.dia;
        this.mes = tempo.mes;
        this.ano = tempo.ano;
        this.minuto = tempo.minuto;
        this.hora = tempo.hora;
        this.semestreAtual = tempo.semestreAtual;
    }

    public getId() { return this.id; }
    public getNome(): string {
        return this.nome;
    }
    public getMatricula(): string {
        return this.matricula;
    }
    public setToken(token: any) {
        this.token = token;
    }
    public getToken() {
        return this.token;
    }
    public getDesempenhoString(): string {
        return `${this.desempenho}`;


    }
    public setProva(indexMateria, indexProjeto, projeto) {
        this.materiasMatriculadas[indexMateria].provas[indexProjeto] = projeto;
    }
    public getProva(indexMateria, indexProva) {
        return this.materiasMatriculadas[indexMateria].provas[indexProva];
    }
    public setTrabalho(indexMateria, indexProjeto, projeto) {
        this.materiasMatriculadas[indexMateria].trabalhos[indexProjeto] = projeto;
    }
    public getTrabalho(indexMateria, indexProva) {
        return this.materiasMatriculadas[indexMateria].trabalhos[indexProva];
    }
    public setId(id: number) {
        this.id = id;
    }
    public getDesempenho(): number {
        return this.desempenho;
    }
    // public setCansaco?(value: number) {
    //     this.cansaco = value;
    // }
    public setDesempenho?(value: number) {
        this.desempenho = value;
    }
    public getData(): Date {
        return new Date(this.ano, this.mes, this.dia, this.hora, this.minuto)
    }
    public getDataString() {
        let day, month;
        if (this.dia < 10) {
            day = '0' + this.dia;
        } else { day = this.dia }
        if (this.mes < 10) {
            month = '0' + this.mes;
        } else { month = this.mes }
        return `${day}/${month}/${this.ano}`;
    }
    public getHorasString() {
        let hour, minute;
        if (this.hora < 10) {
            hour = '0' + this.hora;
        } else { hour = this.hora; }
        if (this.minuto < 10) {
            minute = '0' + this.minuto;
        } else { minute = this.minuto }
        return `${hour}:${minute}`;
    }
    public getBirthday() {
        return this.aniversario;
    }
    public setHorasDeEstudo(value: number) {
        this.ultimaAcao = { key: 'estudo', horasDeEstudo: value };
    }
    public setHorasDeLazer(value: number) {
        this.ultimaAcao = { key: 'lazer', horasDeLazer: value };
    }
    public setHorasDeSono(value: number) {
        this.ultimaAcao = { key: 'sono', horasDeSono: value };
    }
    public setHorasAula(value: number) {
        this.ultimaAcao = { key: 'aula', horasDeAula: value }
    }
    public getUltimaAcaoKey() {
        return this.ultimaAcao.key;
    }

    public setPausa() {
        this.ultimaAcao = { key: 'pausa', pausa: true };
    }
    public getUltimaAcao() {
        return this.ultimaAcao;
    }
    public pushMateriaRealizada?(materia: Materia) {
        this.materiasRealizadas.push(materia);
    }
    public pushMateriaMatriculada?(materia: Materia) {
        this.materiasMatriculadas.push(materia);
    }
    public pushMateriasMatriculadas?(materias: Array<Materia>) {
        materias.forEach(materia => {

            this.pushMateriaMatriculada(materia)
        });
    }
    public clearMateriasMatriculadas() {
        this.materiasMatriculadas = [];
    }
    public pushHistoricoDeAcoes(acao: any) {
        this.historicoDeAcoes.push(acao);
    }
    public setFaltas(value: number) {
        this.faltas = value;
    }
    public getAulaHoje() {
        return this.aulaHoje;
    }
    public getFaltas() {
        this.faltas;
    }
    public increaseFaltas() {
        this.faltas++;
    }
    public naoIrAulaHoje()
    {
        this.aulaHoje = false;
    }
    public irParaAula() {
        this.aulaHoje = true;
    }
}