import { Trabalho } from "./trabalho"
import { Prova } from "./prova";

export class Materia {
    nome: string;
    orgao: string;
    mencao?: string;
    creditos: number;
    obrigatoria?: boolean;
    provas?: Array<Prova>;
    trabalhos?: Array<Trabalho>
    preRequisitos?: Array<string>;  //Usar o nome da matéria apenas.

}

export class MateriasObrigatorias {
    materias: Array<Materia>;
}

export class MateriasOptativas {
    materias: Array<Materia>;
}