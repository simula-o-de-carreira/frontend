export class Tempo {
    semestreAtual?: number;
    dia?: number;
    mes?: number;
    ano?: number;
    hora?: number;
    minuto?: number;

    public getSemestre?() {
        return this.semestreAtual;
    }
    public getDia?() {
        return this.dia;
    }
    public getMes?() {
        return this.mes;
    }
    public getAno?() {
        return this.ano;
    }
    public getHora?() {
        return this.hora;
    }
    public getMinuto?() {
        return this.minuto;
    }
    public setSemestreAtual?(value: number) {
        this.semestreAtual = value;
    }
    public setDay?(value: number) {
        this.dia = value
    }
    public setMonth?(value: number) {
        this.mes = value;
    }
    public setYear?(value: number) {
        this.ano = value;
    }
    public setHour?(value: number) {
        this.hora = value;
    }
    public setMinutes?(value: number) {
        this.minuto = value;
    }



}