import { Injectable } from '@angular/core';
import { Usuario } from './models/usuario';
import { Materia } from './models/materia';

@Injectable({
  providedIn: 'root'
})
export class ActionsService {

  constructor() { }
  public usuario: Usuario;
  materiaSelecionada;
  private dataPoints: Array<any> = [];

  public study(event) { // algoritmo para calcular o desempenho apos estudo

    if (!this.materiaSelecionada) {
      alert("Selecione uma tópico para estudar!");
    } else {
      let c = confirm(`Tem certeza que deseja estudar para o ${this.materiaSelecionada.nome} ${this.materiaSelecionada.indexProjeto + 1} por ${event} hora(s)?`);
      console.log(this.materiaSelecionada);

      if (c) {
        this.usuario.setHorasDeEstudo(event);
        this.atualizarProjeto(event);
        this.calcularDesempenhoEstudo(event);
      }
    }

  }
  public class(event) {
    this.convertDecimaltoHours(event);
    // this.calcularDesempenhoAula(event - (event / 2));
    this.usuario.setHorasAula(event);
  }
  public break(event) { //algoritmo para calcular a pausa
    if ((this.usuario.getMinuto() + event) > 60) {
      this.calcularTempo(1);
    }
    this.calcularDesempenhoPausa();
    // this.calcularCansacoPausa();
    this.usuario.setPausa();

    this.usuario.setMinutes((this.usuario.getMinuto() + event) % 60)
  }
  public fun(value) { // algoritmo para calcular o desempenho apos lazer

    let c = confirm(`Tem certeza que deseja passar ${value} hora(s) se com lazer?`);
    if (c) {
      this.usuario.setHorasDeLazer(value);
      this.calcularTempo(value);
      this.calcularDesempenhoLazer(value);
    }
  }
  public setDataPoints(dataPoints) {
    this.dataPoints = dataPoints;
  }
  public getDataPoints() {
    return this.dataPoints;
  }
  public sleep(value) {
    let c = confirm(`Tem certeza que deseja dormir por ${value} hora(s)?`);
    if (c) {
      this.calcularTempo(value);
      this.calcularDesempenhoSono(value);
      this.usuario.setHorasDeSono(value);
    }
  }
  private calcularDesempenhoSono(value) {
    if (this.usuario.getHora() >= 23 && this.usuario.getHora() < 6) {
      if (value > 6 && value < 9) {
        this.usuario.setDesempenho(1);
      }
      else if (value >= 9) {
        this.usuario.setDesempenho(
          this.usuario.getDesempenho() * 1.25
        );
      }
      else if (value <= 6) {
        this.usuario.setDesempenho(
          this.usuario.getDesempenho() * 0.75
        );
      }
    }
    else {
      this.usuario.setDesempenho(
        this.usuario.getDesempenho() * 1.50
      );
    }
  }
  private calcularDesempenhoLazer(value) {
    if (this.usuario.getDesempenho() === 0) {
      this.usuario.setDesempenho(0.2);
    }
    for (let i = 0, n = 1; i < value; i++ , n++) {
      this.usuario.setDesempenho(
        (Math.round((this.usuario.getDesempenho() * Math.pow(1.1, n)) * 100) / 100)
      );
      this.dataPoints.push({ x: this.usuario.getHora(), y: this.usuario.getDesempenho() });

    }
    if (this.usuario.getDesempenho() > 2) {
      this.usuario.setDesempenho(2);
    }
  }
  private calcularDesempenhoPausa() {

    if (this.usuario.getUltimaAcaoKey() === 'pausa') {
      this.usuario.setDesempenho(
        this.usuario.getDesempenho() + (0.025 * this.usuario.getDesempenho())
      )
    }

    else if ((this.usuario.getUltimaAcaoKey() === 'estudo' || this.usuario.getUltimaAcaoKey() === 'aula') && this.usuario.getUltimaAcao().horasDeEstudo === 1) {
      this.usuario.setDesempenho(
        (Math.round((this.usuario.getDesempenho() * Math.pow(1.2, 2)) * 100) / 100)
      );
    }

    else {

      this.usuario.setDesempenho(
        this.usuario.getDesempenho() + (0.05 * this.usuario.getDesempenho())
      );

    }
    if (this.usuario.getDesempenho() > 2) {
      this.usuario.setDesempenho(2);
    }
  }
  // private calcularCansacoPausa() {
  //   if (this.usuario.getCansaco() === 0) {
  //     this.usuario.setCansaco(0.5)
  //   }
  //   else { this.usuario.setCansaco(this.usuario.getCansaco() + this.usuario.getCansaco() * 0.005); }
  // }
  private calcularDesempenhoEstudo(value) {
    this.calcularTempo(value);
    for (let i = 0, n = 1; i < value; i++ , n++) {
      this.usuario.setDesempenho(
        (Math.round((this.usuario.getDesempenho() * Math.pow(0.9, n)) * 100) / 100)
      );
    }
    if (this.usuario.getDesempenho() > 2) {
      this.usuario.setDesempenho(2);
    }
  }
  private calcularDesempenhoAula(value) {
    for (let i = 0, n = 1; i < value; i++ , n++) {
      this.usuario.setDesempenho(
        (Math.round((this.usuario.getDesempenho() * Math.pow(0.9, n)) * 100) / 100)
      );
    }
    if (this.usuario.getDesempenho() > 2) {
      this.usuario.setDesempenho(2);
    }
  }

  private calcularTempo(value) {

    if (this.usuario.getHora() + value > 24) {
      if (this.usuario.getDia() + 1 > 30) {
        if (!this.usuario.getAulaHoje()) {
          this.usuario.increaseFaltas();
          this.usuario.naoIrAulaHoje();
        }
        if (this.usuario.getMes() + 1 > 12) {
          this.usuario.setYear((this.usuario.getAno()) + 1);
        }
        this.usuario.setMonth((this.usuario.getMes() + 1) % 24)
      }
      this.usuario.setDay((this.usuario.getDia() + 1) % 30);
    }
    this.usuario.setHour((value + this.usuario.getHora()) % 24);

  }
  public setUsuario(user: Usuario) {
    this.usuario = user;
  }
  public setMateriaSelecionada(materia: Materia) {
    this.materiaSelecionada = materia;
  }
  public getUsuario() {
    return this.usuario;
  }
  private convertDecimaltoHours(horas) {
    let hour = Math.floor(horas);
    console.log(hour);

    this.calcularTempo(hour);
    let minutos = Math.floor((horas % 1) * 60);
    if ((this.usuario.getMinuto() + minutos) > 60) {
      this.calcularTempo(1);
    }
    this.usuario.setMinutes((this.usuario.getMinuto() + minutos) % 60)

  }

  protected atualizarProjeto(value) {
    let string = 'provas';
    let projeto;
    if (this.materiaSelecionada.nome === 'trabalho') {
      string = 'trabalhos'
      projeto = this.usuario.getTrabalho(this.materiaSelecionada.indexMateria, this.materiaSelecionada.indexProjeto)
    } else { string = 'provas', projeto = this.usuario.getProva(this.materiaSelecionada.indexMateria, this.materiaSelecionada.indexProjeto) }
    console.log('projeto pre conta', projeto);


    let desempenho = this.usuario.getDesempenho();
    let horasRestantes = (projeto.horasDeEstudoNecessario - projeto.horasEstudadas) / (desempenho);
    console.log('horas restantes, projeto estudo necessario, projeto horas, horasRestantes', horasRestantes);

    if (Math.floor(horasRestantes - value) <= 0) {
      projeto.mencao = this.subirMencao(projeto.mencao);
      projeto.horasEstudadas = (value - horasRestantes)

    } else { projeto.horasEstudadas = (projeto.horasEstudadas + value) }
    console.log('projeto pos conta', projeto);


    if (projeto.mencao === 'SS') {
      projeto.realizado = true;
    }
    if (string === 'provas') {
      this.usuario.setProva(this.materiaSelecionada.indexMateria, this.materiaSelecionada.indexProjeto, projeto);
    } else {
      this.usuario.setTrabalho(this.materiaSelecionada.indexMateria, this.materiaSelecionada.indexProjeto, projeto);

    }

  }
  public subirMencao(mencao: string) {
    if (mencao === 'SR') {
      return 'MI'
    }
    else if (mencao === 'MI') {
      return 'MM'
    }
    else if (mencao === 'MM') {
      return 'MS'
    }
    else if (mencao === 'MS') {
      return 'SS'
    }
  }
}



