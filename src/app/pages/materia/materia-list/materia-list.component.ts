import { Component, OnInit, Input, Inject } from '@angular/core';
import { Materia } from 'src/app/models/materia';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';

@Component({
  selector: 'app-materia-list',
  templateUrl: './materia-list.component.html',
  styleUrls: ['./materia-list.component.css']
})
export class MateriaListComponent implements OnInit {

  constructor(public dialogRef: MatDialogRef<MateriaListComponent>, @Inject(MAT_DIALOG_DATA) public materias: any) { }

  ngOnInit() {
    // console.log(this.materias);

  }

}
