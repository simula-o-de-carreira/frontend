import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Usuario } from '../models/usuario';
import { environment } from 'src/environments/environment';
import { MateriaService } from './materia.service';

@Injectable({
  providedIn: 'root'
})
export class MatriculaService {

  constructor(protected http: HttpClient) { }


  public register(usuario: Usuario) {
    let user = {
      name: usuario.getNome(),
      birthday: usuario.getBirthday()
    }
 
    return this.http.post(`${environment.apiUrl}/user`, user);
  }


}
