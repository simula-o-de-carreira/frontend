import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialog } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, protected route: Router, public dialogRef: MatDialog) { }
  id: number;
  ngOnInit() {
    if (this.data) {
      this.id = this.data;
    }
  }
  public startGame() {
    this.route.navigate([`main-panel/${this.id}`]);
    this.dialogRef.closeAll();
  }

}
