import { Component, OnInit, Output, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Location } from '@angular/common';
import { Usuario } from 'src/app/models/usuario';
import { Tempo } from 'src/app/models/tempo';
import { EventEmitter } from '@angular/core';
import { MatriculaService } from '../matricula.service';
import { timeout } from 'q';
import { CacheService } from '../cache.service';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MessageComponent } from './message/message.component';
import { MateriaService } from '../materia.service';


@Component({
  selector: 'app-matricula',
  templateUrl: './matricula.component.html',
  styleUrls: ['./matricula.component.css']
})
export class MatriculaComponent implements OnInit {
  public userForm: FormGroup;
  public loading: boolean = false;
  constructor(private location: Location, protected materiaService: MateriaService, protected dialogRef: MatDialog, private router: Router, public matriculaService: MatriculaService, public cacheService: CacheService) { }

  @Output() usuarioEmit = new EventEmitter;
  public startDate = new Date(1998, 11, 22);
  public message: boolean = false;
  private usuario: Usuario;
  private tempo: Tempo = { semestreAtual: 1, dia: 1, mes: 3, hora: 8, minuto: 0, ano: 2020 };

  ngOnInit() {
    this.userForm = new FormGroup({
      nome: new FormControl('', Validators.required),
      matricula: new FormControl('', Validators.required),
      birthday: new FormControl('', Validators.required)
    });
  }
  public hasError(controlName: string, errorName: string) {
    return this.userForm.controls[controlName].hasError(errorName);
  }

  public onCancel() {
    this.location.back();
  }
  public createUser(userFormValue) {
    if (this.userForm.valid) {
      this.executeUserCreation(userFormValue);
      this.materiaService
      this.emitUser(this.getUsuario());
      this.matriculaService.register(this.usuario).subscribe(
        (data: any) => {
          this.loading = true;
          console.log(data['id']);
          this.usuario.setId(data['id'])
          this.cacheService.setUserToCache(data['id'], this.usuario);
          this.dialogRef.open(
            MessageComponent, { height: "900px", width: "900px", data: data['id'], },

          );
        }
      );

    }

  }
  private emitUser(user: Usuario) {
    this.usuarioEmit.emit(user);
  }
  private executeUserCreation(userFormValue) {
    let user = new Usuario();
    user.super(userFormValue.nome, userFormValue.matricula, userFormValue.birthday, 1, this.tempo)
    user.setDesempenho(1);
    console.log('usuario criado', user);
    this.setUser(user);

  }
  private setUser(user: Usuario) {
    this.usuario = user;

  }
  private generateMatricula() {
    this.userForm.patchValue(
      {
        matricula: '20/' + Math.floor(Math.random() * 10000000).toString()
      }
    )
  }
  private getUsuario(): Usuario {
    return this.usuario;
  }
  private getTempo(): Tempo {
    return this.tempo;
  }
}