import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LoadGameService {

  constructor(private http: HttpClient) { }


  public listUsers() {
    return this.http.get(`${environment.apiUrl}/saves`);
  }
  public getToken(id: number) {
    let body = {
      userId: id
    }
    return this.http.post(`${environment.apiUrl}/login`, body);
  }
}
