import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import { CacheService } from '../cache.service';
import { MateriaService } from '../materia.service';

@Component({
  selector: 'app-new-semester',
  templateUrl: './new-semester.component.html',
  styleUrls: ['./new-semester.component.css']
})
export class NewSemesterComponent implements OnInit {

  constructor(protected activatedRoute: ActivatedRoute, protected cacheService: CacheService, private route: Router, protected materiaService: MateriaService) { }
  idSelected;
  activeVariable = [];
  somaCreditos: number = 0;
  abrirMateriasRealizadas: boolean = false;
  usuario: Usuario;
  allMaterias: any = [];
  ngOnInit() {
    if (localStorage.getItem('token')) {
      let token = localStorage.getItem('token');
      let users;
      for (let i = 0; i < 250; i++) {
        let id = `${i}`
        if (localStorage.getItem(id)) {
          if (JSON.parse(localStorage.getItem(id))['token'] == token) {
            console.log(JSON.parse(localStorage.getItem(id))['token']);

            console.log(this.usuario);

            this.usuario = this.cacheService.getUserFromCache(i);
            i = 250;
          }
        }
      }
      this.materiaService.getMaterias().subscribe(
        materias => {
          materias.forEach(materia => {
            this.allMaterias.push(this.materiaService.montarMateria(materia));
          });
        }
      );

    } else {
      this.route.navigate(['/load-game'])
    }
  }
  selectCard(materia) {
    this.somaCreditos = this.somaCreditos + materia.creditos;
    this.activeVariable.push(materia);
  }
  public isSelected(materia) {
    this.activeVariable.forEach(materiaAtiva => {
      if (materia.nome === materiaAtiva.nome) {
        return true;
      }
    })
    return false;
  }
}
