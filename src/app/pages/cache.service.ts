import { Injectable } from '@angular/core';
import { Usuario } from '../models/usuario';
import { Tempo } from '../models/tempo';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  constructor() { }

  public setUserToCache(id: number, user: Usuario) {
    console.log(id);

    localStorage.setItem(id.toString(), JSON.stringify(user));
  }
  public getUserFromCache(id: number): Usuario {

    return this.setCacheToUser(JSON.parse(localStorage.getItem(id.toString())));
  }
  public setCacheToUser(cacheParse) {
    let user = new Usuario();
    console.log('cacheparse', cacheParse);
    let tempo = new Tempo();
    tempo = {
      semestreAtual: cacheParse.semestreAtual,
      dia: cacheParse.dia,
      mes: cacheParse.mes,
      hora: cacheParse.hora,
      minuto: cacheParse.minuto,
      ano: cacheParse.ano
    };
    console.log('tempo cache', tempo);

    user.super(cacheParse.nome, cacheParse.matricula, cacheParse.aniversario, cacheParse.desempenho, tempo,
      (cacheParse.materiasRealizadas ? cacheParse.materiasRealizadas : []),
      (cacheParse.materiasMatriculadas ? cacheParse.materiasMatriculadas : [])
    )
    user.token = cacheParse.token;
    user.faltas = cacheParse.faltas;
    cacheParse.desempenhoDiario ? user.desempenhoDiario = cacheParse.desempenhoDiario : [];
    cacheParse.aulaHoje ? user.aulaHoje = cacheParse.aulaHoje : user.aulaHoje = false;
    return user;
  }
}
