import { Component, OnInit } from '@angular/core';
import { LoadGameService } from '../load-game.service';
import { Title } from '@angular/platform-browser';
import { RouterLink, Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import { CacheService } from '../cache.service';

@Component({
  selector: 'app-load-game',
  templateUrl: './load-game.component.html',
  styleUrls: ['./load-game.component.css']
})
export class LoadGameComponent implements OnInit {

  constructor(protected loadGameService: LoadGameService, private title: Title, private route: Router, protected cacheService: CacheService) { }
  listaUsers = [];
  usuarioSelecionado: Usuario;
  loading: boolean = true;
  ngOnInit() {
    this.title.setTitle('Carregar jogos');
    this.loadGameService.listUsers().subscribe(
      (data: any) => {
        data.forEach(user => {
          this.listaUsers.push(user);
          this.loading = false;
          console.log(user);

        });
      }
    )
  }
  selectStudent(id: number) {
    console.log(id);

    this.loadGameService.getToken(id).subscribe(
      data => {
        this.loading = true;
        this.usuarioSelecionado = this.cacheService.getUserFromCache(id);
        this.usuarioSelecionado.setId(id);
        this.usuarioSelecionado.setToken(data);
        console.log('user', this.usuarioSelecionado);
        console.log('token', data);
        document.cookie = `token=${data}`;
        localStorage.setItem('token', data.toString());
        this.route.navigate([`/main-panel/${id}`]);

      }
    )

  }

}
