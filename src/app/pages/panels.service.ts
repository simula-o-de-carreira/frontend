import { Injectable } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PanelsService {

  constructor(private http: HttpClient) { }

  public getMateriasMatriculadas(token: any) {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': `Bearer ${token}`
    });
    let options = {
      headers: headers
    }
    return this.http.get(`${environment.apiUrl}/current-disciplinas`, options);

  }

}
