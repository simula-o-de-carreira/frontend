import { Component, OnInit, Input, EventEmitter, Output, OnChanges } from '@angular/core';
import { PanelsService } from '../../panels.service';

@Component({
  selector: 'app-panels-materia',
  templateUrl: './panels-materia.component.html',
  styleUrls: ['./panels-materia.component.css']
})
export class PanelsMateriaComponent implements OnInit, OnChanges {
  constructor(protected panelService: PanelsService) { }
  @Input() token;
  @Input() faltas: number
  @Input() aulaRealizada;
  @Input() diaSeguinte?: boolean;
  @Output() emitClass = new EventEmitter();
  loading = true;
  horasMedia: number;
  materias = [];
  ngOnInit() {

    window.setTimeout(
      () => {
        if (this.token) {
          this.loading = false;
          this.panelService.getMateriasMatriculadas(this.token).subscribe((data: any) => {
            this.materias = data;
            console.log(this.materias);
            this.horasMedia = this.getHorasMedias(this.materias);
          });
          window.clearTimeout();


        }
      }, 100
    );

  }
  ngOnChanges() {
    if (this.diaSeguinte) {
      this.aulaRealizada = false;
    }
  }
  protected class() {
    let c = confirm(`Tem certeza que deseja estudar por ${this.horasMedia} horas?`);
    if (c) {
      this.aulaRealizada = true;
      this.emitClass.emit({ key: "aula", value: this.horasMedia });
    }

  }
  protected getFaltasPermitidas() {
    let faltasPermitidas = Math.floor(this.horasMedia * 0.25 * 16) //16 Semanas
    return faltasPermitidas;
  }
  protected getHorasMedias(materias: any) {
    let hora = 0;
    materias.forEach(materia => {
      hora = hora + materia.cr;
    });
    hora = hora / 5;
    console.log(hora);

    return hora
  }
}
