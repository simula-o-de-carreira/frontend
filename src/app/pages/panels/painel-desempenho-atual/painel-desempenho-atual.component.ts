import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as CanvasJS from './canvasjs.min'
import { Usuario } from 'src/app/models/usuario';
// let CanvasJS = require('canvasjs');

@Component({
  selector: 'app-painel-desempenho-atual',
  templateUrl: './painel-desempenho-atual.component.html',
  styleUrls: ['./painel-desempenho-atual.component.css']
})
export class PainelDesempenhoAtualComponent implements OnInit, OnChanges {
  // constructor() { }

  @Input() dataPoints: any;
  ngOnInit() {
    this.updateChart();
  }
  ngOnChanges() {
    console.log('change');

    this.updateChart();
  }
  private updateChart() {
    if (this.dataPoints) {
      let chart = new CanvasJS.Chart("chartContainer", {
        zoomEnabled: true,
        animationEnabled: true,
        // exportEnabled: true,
        title: {
          text: "Desempenho Estudo x Dia"
        },
        subtitles: [{
          text: ""
        }],
        data: [
          {
            type: "line",
            dataPoints: this.dataPoints
          }]
      });

      chart.render();
    }
  }
}
