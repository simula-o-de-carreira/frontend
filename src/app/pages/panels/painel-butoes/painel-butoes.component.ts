import { Component, OnInit, Output } from '@angular/core';
import { coerceNumberProperty } from '@angular/cdk/coercion';
// import { EventEmitter } from 'events';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-painel-butoes',
  templateUrl: './painel-butoes.component.html',
  styleUrls: ['./painel-butoes.component.css']
})
export class PainelButoesComponent implements OnInit {
  @Output() emmiter = new EventEmitter;
  constructor() { }

  valueHoras: number = 1;
  ngOnInit() {
  }

  set tickInterval(value) {
    this._tickInterval = coerceNumberProperty(value);
  }
  private _tickInterval = 1;

  study() {
    this.emmiter.emit({ key: 'study', value: this.valueHoras });
  }
  break() {
    this.emmiter.emit({ key: 'pausa', value: 10 });
  }
  fun() {
    this.emmiter.emit({ key: 'lazer', value: this.valueHoras });
  }
  sleep() {
    this.emmiter.emit({ key: 'sono', value: this.valueHoras });
  }
  setValueHoras(event: any) {
    this.valueHoras = event.value;
  }
}
