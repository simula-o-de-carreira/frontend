import { Component, OnInit, Output, Input, OnChanges } from '@angular/core';
import { Materia } from '../../../models/materia';
import { PanelsService } from '../../panels.service';
import { EventEmitter } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { ActionsService } from 'src/app/actions.service';



@Component({
  selector: 'app-painel-pendente',
  templateUrl: './painel-pendente.component.html',
  styleUrls: ['./painel-pendente.component.css']
})
export class PainelPendenteComponent implements OnInit, OnChanges {

  @Output() materiaSelecionada = new EventEmitter
  @Input() materias: any;
  @Input() desempenho: any;
  constructor(private panelService: PanelsService, protected actionsService: ActionsService) { }
  activeVariable: any;

  ngOnInit() {

    console.log(this.materias)


  }
  selectCard(indexMateria, projeto, index, nome) {
    this.activeVariable = projeto;
    this.materiaSelecionada.emit({ projeto: projeto, indexMateria: indexMateria, indexProjeto: index, nome: nome });


  }
  ngOnChanges() {
  }
  calculaHoraRestante(projeto) {
    
    return ((projeto.horasDeEstudoNecessario -
      projeto.horasEstudadas) / this.desempenho).toFixed(2)
  }
}
