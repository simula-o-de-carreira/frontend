import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PainelPrincipalComponent } from './painel-principal.component';
import { PainelPendenteComponent } from './painel-pendente/painel-pendente.component';
import { MaterialModule } from '../../material/material.module';
import { Routes, RouterModule } from '@angular/router';
import { PainelFeitoComponent } from './painel-feito/painel-feito.component';
import { PanelsMateriaComponent } from './panels-materia/panels-materia.component';
// import { PainelDesempenhoTotalComponent } from './painel-desempenho-total/painel-desempenho-total.component';
// import { PainelButoesComponent } from './painel-butoes/painel-butoes.component';

const appRoutes: Routes = [


]

@NgModule({
  declarations: [

    // PainelButoesComponent
  // PainelDesempenhoTotalComponent
// PanelsMateriaComponent
],
  imports: [
    MaterialModule,
    RouterModule.forChild(appRoutes)
  ]
})
export class PanelsModule { }
