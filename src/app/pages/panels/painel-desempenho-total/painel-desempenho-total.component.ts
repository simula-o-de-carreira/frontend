import { Component, OnInit } from '@angular/core';
import * as CanvasJS from './canvasjs.min'

@Component({
  selector: 'app-painel-desempenho-total',
  templateUrl: './painel-desempenho-total.component.html',
  styleUrls: ['./painel-desempenho-total.component.css']
})
export class PainelDesempenhoTotalComponent implements OnInit {

  constructor() { }
  ngOnInit() {
    let dataPoints = [];
    let y = 0;
    for (var i = 0; dataPoints.length <= 5; (i + 0.2)) {
      y = Math.random() * (2 - 0) + 0;
      dataPoints.push({ y: y });
    }
    let chart = new CanvasJS.Chart("chartTotalContainer", {
      zoomEnabled: true,
      animationEnabled: true,
      // exportEnabled: true,
      title: {
        text: "Média de desempenho por semestre"
      },
      subtitles: [{
        text: ""
      }],
      data: [
        {
          type: "line",
          dataPoints: dataPoints
        }]
    });

    chart.render();

  }

}
