import { Component, OnInit, Input } from '@angular/core';
import { PanelsService } from '../../panels.service';
import { Materia } from '../../../models/materia';

@Component({
  selector: 'app-painel-feito',
  templateUrl: './painel-feito.component.html',
  styleUrls: ['./painel-feito.component.css']
})
export class PainelFeitoComponent implements OnInit {

  constructor(private panelService: PanelsService) { }
  @Input() materias: Array<Materia>
  activeVariable: any;
  ngOnInit() {
    // this.materias = this.panelService.getMaterias();
  }
  selectCard(value) {
    this.activeVariable = value;

  }
}
