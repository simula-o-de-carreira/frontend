import { Component, OnInit, Input, Output, OnChanges } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { Materia } from '../../models/materia';
import { Location } from '@angular/common';
import { Usuario } from 'src/app/models/usuario';
import { Tempo } from 'src/app/models/tempo';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { HistoricoComponent } from '../historico/historico.component';
import { ActionsService } from 'src/app/actions.service';
import * as CanvasJS from './canvasjs.min'
import { MateriaService } from '../materia.service';
import { MateriaListComponent } from '../materia/materia-list/materia-list.component';
import { CacheService } from '../cache.service';
import { ActivatedRoute, Router } from '@angular/router';
import { LoadGameService } from '../load-game.service';
import { PanelsService } from '../panels.service';
@Component({
  selector: 'app-painel-principal',
  templateUrl: './painel-principal.component.html',
  styleUrls: ['./painel-principal.component.css']
})
export class PainelPrincipalComponent implements OnInit {

  constructor(public materiaService: MateriaService,
    private activatedRoute: ActivatedRoute,
    public cacheService: CacheService,
    protected title: Title,
    protected route: Router,
    protected dialog: MatDialog,
    protected actionService: ActionsService,
    protected loadGameService: LoadGameService,
    protected panelService: PanelsService) {
  }
  private id: number;
  public usuario: Usuario;
  private materiaSelecionada: Materia;
  public aulaRealizada: boolean;
  private materiasCurso: Array<Materia>
  private dataPoints: Array<any> = [];
  private idSelected;
  ngOnInit() {
    this.title.setTitle("Painel Principal");
    this.activatedRoute.paramMap.subscribe(params => {
      if (params) {
        this.idSelected = params.get("id").valueOf();
        this.usuario = this.cacheService.getUserFromCache(this.idSelected);
        this.usuario.setId(this.idSelected);
        this.loadGameService.getToken(this.idSelected).subscribe(
          data => {
            this.usuario.setToken(data);
            localStorage.setItem('token', data.toString());
            if (this.usuario.materiasMatriculadas.length === 0) {
              this.panelService.getMateriasMatriculadas(data).subscribe(
                materias => {
                  let materiasNovas = this.materiaService.matricular(materias)
                  this.usuario.pushMateriasMatriculadas(materiasNovas);
                  console.log(this.usuario);
                  this.cacheService.setUserToCache(this.usuario.id, this.usuario);

                }
              );
            }
          }
        );
        window.onload = () => {
          this.materiaService.getMaterias().subscribe(
            data => {
              this.materiasCurso = this.materiaService.loadAll(data);
            }
          );

          this.updateChart();
        }
        this.dataPoints.push({ x: this.usuario.getHora(), y: this.usuario.getDesempenho() });
      }
      else {
        this.route.navigate(['/load-game']);
      }
    });


  }

  public onCancel() {
    let c = confirm("Tem certeza que deseja sair sem salvar?");
    if (c) { window.close(); }
  }
  public openHistory() {
    this.dialog.open(HistoricoComponent, {
      height: '400px',
      width: '600px',
      data: this.usuario.historicoDeAcoes
    });

  }
  public openClasses() {
    this.dialog.open(MateriaListComponent, {
      height: '400px',
      width: '600px',
      data: this.materiasCurso
    });

  }
  public actions(event) {
    this.actionService.setUsuario(this.usuario);
    let diaAtual = this.usuario.getDia();
    this.actionService.setDataPoints(this.dataPoints);
    if (event.key === 'study') {
      this.actionService.setMateriaSelecionada(this.materiaSelecionada);
      this.actionService.study(event.value);
    }
    else if (event.key === 'pausa') {
      this.actionService.break(event.value);
    }
    else if (event.key === 'lazer') {
      this.actionService.fun(event.value);
    }
    else if (event.key === 'sono') {
      this.actionService.sleep(event.value);
    }
    else if (event.key === 'aula') {
      this.usuario.irParaAula();
      this.aulaRealizada = true;
      this.actionService.class(event.value)
    }
    this.usuario = this.actionService.getUsuario();
    this.dataPoints = this.actionService.getDataPoints()
    if (this.usuario.getDia() > diaAtual) {
      this.aulaRealizada = false;
      this.dataPoints = [];
      this.actionService.setDataPoints(this.dataPoints);

    }
    this.dataPoints.push({ x: this.usuario.getHora(), y: this.usuario.getDesempenho() });


    this.updateChart();

    if (this.usuario.getHora() > 23 || this.usuario.getHora() < 5) {
      this.usuario.setDesempenho(0.5);
    }



    this.usuario.pushHistoricoDeAcoes(this.usuario.getUltimaAcao());
  }
  public save() {
    let c = confirm("Tem certeza ue deseja salvar?");
    if (c) {
      this.cacheService.setUserToCache(this.usuario.id, this.usuario);
    }
  }
  public trancarSemestre() {
    let c = confirm("Você tem certeza que deseja trancar o semestre e escolher as próximas disciplinas?");
    if (c) {
      this.route.navigate(['/new-semester']);
    }
  }
  public getMateriaSelecionada(event) {
    this.materiaSelecionada = event;
  }
  public userCadastro(event) {
    this.usuario = event;
    this.cacheService.setUserToCache(this.usuario.id, this.usuario);
    console.log(event);

    this.title.setTitle("Painel Principal");
  }





  private updateChart() {
    if (this.dataPoints) {
      let chart = new CanvasJS.Chart("chartContainer", {
        zoomEnabled: true,
        animationEnabled: true,
        // exportEnabled: true,
        title: {
          text: "Desempenho Estudo x Hora"
        },
        subtitles: [{
          text: ""
        }],
        data: [
          {
            type: "line",
            dataPoints: this.dataPoints
          }]
      });

      chart.render();
    }
  }
}
