import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Materia } from '../models/materia';
@Injectable({
  providedIn: 'root'
})
export class MateriaService {

  constructor(private http: HttpClient) { }

  public getMaterias(): Observable<any> {
    let headers = new HttpHeaders({
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'GET,POST,OPTIONS,DELETE,PUT'

    });
    let options = { headers: headers }
    return this.http.get(`${environment.apiUrl}/disciplinas`, options);
  }
  public loadAll(materiasApi: any): Array<Materia> {
    let materia = [];
    console.log(Materia);

    materiasApi.forEach(materiaApi => {
      materia.push(
        this.montarMateria(materiaApi)
      )
    });

    return materia;
  }

  public montarMateria(aula) {
    let materia = {
      id: aula.id,
      nome: aula.nome,
      creditos: aula.cr,
      orgao: aula.orgao,
      mencao: 'SR',
      obrigatoria: aula.pertenceAoCursoID,
      provas: this.montarProvas(aula.qtnProvas, aula.qntHorasProva),
      trabalhos: this.montarTrabalhos(aula.qtnTrabalhos, aula.qntHorasTrabalho),
    }
    return materia;
  }
  protected montarProvas(quantidadeProvas: number, quantidadeHorasProva) {
    let provas = [];
    for (let i = 0; i < quantidadeProvas; i++) {
      provas.push({
        mencao: 'SR',
        realizado: false,
        horasDeEstudoNecessario: quantidadeHorasProva,
        horasEstudadas: 0
      });
    }
    return provas;
  }

  protected montarTrabalhos(quantidadeTrabalhos: number, quantidadeHorasTrabalho: number) {
    let trabalhos = [];
    for (let i = 0; i < quantidadeTrabalhos; i++) {
      trabalhos.push({
        mencao: 'SR',
        realizado: false,
        horasDeEstudoNecessario: quantidadeHorasTrabalho,
        horasEstudadas: 0
      });
    }
    return trabalhos;
  }
  public matricular(materias) {
    let materiasMatriculadas = [];
    materias.forEach(materia => {

      materiasMatriculadas.push(this.montarMateria(materia))
    });
    return materiasMatriculadas;
  }
}
