import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HomeComponent } from './pages/home/home-component.component';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatGridListModule } from '@angular/material/grid-list';
import { PanelsModule } from './pages/panels/panels.module';
import { MaterialModule } from './material/material.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { PainelPendenteComponent } from './pages/panels/painel-pendente/painel-pendente.component';
import { PainelPrincipalComponent } from './pages/panels/painel-principal.component';
import { MatIconModule } from '@angular/material/icon';
import { MatDividerModule } from '@angular/material/divider';
import { PainelButoesComponent } from './pages/panels/painel-butoes/painel-butoes.component';
import { MatSliderModule } from '@angular/material/slider';
import { PainelFeitoComponent } from './pages/panels/painel-feito/painel-feito.component';
import { PainelDesempenhoAtualComponent } from './pages/panels/painel-desempenho-atual/painel-desempenho-atual.component';
import { PainelDesempenhoTotalComponent } from './pages/panels/painel-desempenho-total/painel-desempenho-total.component';
import { MatriculaComponent } from './pages/matricula/matricula.component';
import { Materia } from './models/materia';
import { MatDatepickerModule } from '@angular/material/datepicker';

import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';
import { MatNativeDateModule } from '@angular/material/core';
import { HttpClientModule } from '@angular/common/http';
import { MatDialogModule } from '@angular/material/dialog';
import { HistoricoComponent } from './pages/historico/historico.component';
import { MateriaListComponent } from './pages/materia/materia-list/materia-list.component';
import { LoadGameComponent } from './pages/load-game/load-game.component';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MessageComponent } from './pages/matricula/message/message.component';
import { PanelsMateriaComponent } from './pages/panels/panels-materia/panels-materia.component';
import { NewSemesterComponent } from './pages/new-semester/new-semester.component';


const appRoutes: Routes = [

  {
    path: '',
    component: HomeComponent,
    data: { title: "Simulação de Carreira - Página Inicial" }
  },
  {
    path: 'new-game',
    component: MatriculaComponent
  },
  {
    path: 'load-game',
    component: LoadGameComponent
  },
  {
    path: 'main-panel/:id',
    component: PainelPrincipalComponent
  },
  {
    path: 'rules',
    component: MessageComponent
  },
  {
    path: 'new-semester',
    component: NewSemesterComponent
  }
]

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    PainelPendenteComponent,
    PainelPrincipalComponent,
    PainelButoesComponent,
    PainelFeitoComponent,
    PainelDesempenhoAtualComponent,
    PainelDesempenhoTotalComponent,
    MatriculaComponent,
    HistoricoComponent,
    MateriaListComponent,
    LoadGameComponent,
    MessageComponent,
    PanelsMateriaComponent,
    NewSemesterComponent,

  ],
  imports: [
    PanelsModule,
    MaterialModule,
    MatGridListModule,
    MatDividerModule,
    MatProgressSpinnerModule,
    MatIconModule,
    MatToolbarModule,
    MatSliderModule,
    MatInputModule,
    MatCardModule,
    MatButtonModule,
    BrowserModule,
    MatFormFieldModule,
    AppRoutingModule,
    HttpClientModule,
    MatDialogModule,
    ReactiveFormsModule,
    MatDatepickerModule,
    MatNativeDateModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes)
  ],
  entryComponents: [
    HistoricoComponent,
    MateriaListComponent,
    MessageComponent

  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
